
function toggleData() {
    fetch('./price.json').then((response) => {
        return response.json();

    }).then((data) => {
        togglePrice(data);

    }).catch((err) => {
        console.log(err);
    });
};
function togglePrice(data) {
    const toggler = document.querySelector(".toggler")
    const basic = document.getElementsByClassName("basic")[0];
    const professional = document.getElementsByClassName("professional")[0];
    const master = document.getElementsByClassName("master")[0];

    if (toggler.checked) {
        basic.innerText = data[0]['monthly']['basic']
        professional.innerText = data[0]['monthly']['professional']
        master.innerText = data[0]['monthly']['master']

    } else {
        basic.innerText = data[0]['annual']['basic']
        professional.innerText = data[0]['annual']['professional']
        master.innerText = data[0]['annual']['master']
    };
};





